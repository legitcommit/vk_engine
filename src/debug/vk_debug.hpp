#pragma once

#ifdef NDEBUG
const bool debug = false;
#else
const bool debug = true;
#endif

class VulkanDebugger
{
public:
    explicit VulkanDebugger();
    ~VulkanDebugger();
};