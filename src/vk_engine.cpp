#include <stdexcept>
#include <sys/types.h>

#include "vk_engine.hpp"

VulkanEngine::VulkanEngine(VkInstance instance) : instance(instance), state(State::UNINITIALIZED)
{
    if (debug)
    {
        debugger = new VulkanDebugger();
    }
}

VulkanEngine::~VulkanEngine()
{
    state = State::SHUTDOWN;
    
    VulkanEngine::cleanup();
}

void VulkanEngine::run()
{
    state = State::INITIALIZING;
    
    if (init())
    {
        state = State::RUNNING;
    }
    else
    {
        throw std::runtime_error("Failed to initialize engine!");
    }

    while (state == State::RUNNING)
    {
        renderLoop();
        gameLoop();
    }
}

//returns true when initialization is finished
bool VulkanEngine::init()
{
    return true;
}

void VulkanEngine::renderLoop()
{
    //renderloop
}

void VulkanEngine::gameLoop()
{
    //gameloop
}

void VulkanEngine::cleanup()
{
    vkDestroyInstance(VulkanEngine::instance, nullptr);
    delete debugger;
}

VkInstance createVkInstance(std::string applicationName, int major, int minor, int patch)
{
    VkApplicationInfo appInfo{};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = applicationName.c_str();
    appInfo.applicationVersion = VK_MAKE_VERSION(major, minor, patch);
    appInfo.pEngineName = "vkEngine";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_3;

    VkInstanceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &appInfo;

    VkInstance instance;

    VkResult result = vkCreateInstance(&createInfo, nullptr, &instance);

    if (result != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create Vulkan instance!");
    }
    return instance;
}