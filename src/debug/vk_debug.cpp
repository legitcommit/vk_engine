#include <iostream>

#include "vk_debug.hpp"

VulkanDebugger::VulkanDebugger()
{
    std::cout << "Debugger created! This must be a debug build.";
}

VulkanDebugger::~VulkanDebugger()
{
    //destruct
}