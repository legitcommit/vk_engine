#pragma once

#include <utility>
#include <sys/types.h>

#include <vulkan/vulkan.h>

class VulkanWindow
{
public:
    explicit VulkanWindow(u_int32_t width, u_int32_t height);
    ~VulkanWindow();
    
    void setWidth(u_int32_t width = 800) {windowSize.first = width;}
    void setHeight(u_int32_t height = 600) {windowSize.second = height;}
    u_int32_t getWidth() {return windowSize.first;}
    u_int32_t getHeight() {return windowSize.second;}
private:
    std::pair<u_int32_t, u_int32_t> windowSize;
};