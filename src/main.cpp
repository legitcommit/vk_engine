#include <cstdlib>
#include <iostream>
#include <ostream>

#include "vk_engine.hpp"

int main()
{
    VulkanEngine* engine = new VulkanEngine();
    int exitCode = EXIT_SUCCESS;

    try{engine->run();}
    catch (...)
    {
        std::cerr << "Failed to run engine. Engine state: " << engine->getStateName() << std::endl;
        exitCode = EXIT_FAILURE;
    }
    delete engine;
    return exitCode;
}