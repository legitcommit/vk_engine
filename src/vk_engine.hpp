#pragma once

#include <map>
#include <string>
#include <sys/types.h>

#include <vulkan/vulkan.h>

#include "debug/vk_debug.hpp"
#include "vk_window.hpp"

VkInstance createVkInstance(std::string applicationName, int major, int minor, int patch);

class VulkanEngine
{
public:
    enum class State
    {
        UNINITIALIZED,
        INITIALIZING,
        RUNNING,
        SHUTDOWN,
    };

    explicit VulkanEngine(VkInstance instance = createVkInstance("hello vulkan", 1, 0, 0));
    ~VulkanEngine();

    VulkanDebugger* getDebugger() const {return debugger;}
    VkInstance getInstance() const {return instance;}
    State getState() {return state;}
    std::string getStateName() {return stateNames[state];}

    void run();

private:
    VkInstance instance;
    VulkanDebugger* debugger;
    VulkanWindow* window;
    State state;

    std::map<State, std::string> stateNames =
    {
        {State::UNINITIALIZED, "UNINITIALIZED"},
        {State::INITIALIZING, "INITIALIZING"},
        {State::RUNNING, "RUNNING"},
        {State::SHUTDOWN, "SHUTDOWN"}
    };

    bool init();
    void renderLoop();
    void gameLoop();
    void cleanup();

    //create a window bro
};